<?php

namespace MiniOrange\OAuth\Controller\Adminhtml\Attrsettings;

use Magento\Backend\App\Action\Context;
use Magento\Customer\Model\ResourceModel\Group\Collection;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\View\Result\PageFactory;
use MiniOrange\OAuth\Helper\OAuthConstants;
use MiniOrange\OAuth\Helper\OAuthMessages;
use MiniOrange\OAuth\Controller\Actions\BaseAdminAction;
use MiniOrange\OAuth\Helper\OAuthUtility;
use Psr\Log\LoggerInterface;

/**
 * This class handles the action for endpoint: mooauth/attrsettings/Index
 * Extends the \Magento\Backend\App\Action for Admin Actions which
 * inturn extends the \Magento\Framework\App\Action\Action class necessary
 * for each Controller class
 */
class Index extends BaseAdminAction implements HttpPostActionInterface, HttpGetActionInterface
{

    private $adminRoleModel;
    private $userGroupModel;

    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        OAuthUtility $oauthUtility,
        ManagerInterface $messageManager,
        LoggerInterface $logger,
        \Magento\Authorization\Model\ResourceModel\Role\Collection $adminRoleModel,
        Collection $userGroupModel
    ) {
        //You can use dependency injection to get any class this observer may need.
        parent::__construct($context, $resultPageFactory, $oauthUtility, $messageManager, $logger);
        $this->adminRoleModel = $adminRoleModel;
        $this->userGroupModel = $userGroupModel;
    }

    /**
     * The first function to be called when a Controller class is invoked.
     * Usually, has all our controller logic. Returns a view/page/template
     * to be shown to the users.
     *
     * This function gets and prepares all our SP config data from the
     * database. It's called when you visis the moaoauth/attrsettings/Index
     * URL. It prepares all the values required on the SP setting
     * page in the backend and returns the block to be displayed.
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        try {
            $params = $this->getRequest()->getParams(); //get params
            
            if ($this->isFormOptionBeingSaved($params)) { // check if form options are being saved
                $this->checkIfRequiredFieldsEmpty(['oauth_am_username'=>$params,'oauth_am_account_matcher'=>$params]);
                $this->processValuesAndSaveData($params);
                $this->oauthUtility->flushCache();
                $this->messageManager->addSuccessMessage(OAuthMessages::SETTINGS_SAVED);
                $this->oauthUtility->reinitConfig();
            }
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
            $this->logger->debug($e->getMessage());
        }
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu(OAuthConstants::MODULE_DIR.OAuthConstants::MODULE_BASE); $resultPage->addBreadcrumb(__('ATTR Settings'), __('ATTR Settings')); 
        $resultPage->getConfig()->getTitle()->prepend(__(OAuthConstants::MODULE_TITLE));
        return $resultPage;
    }


    /**
     * Process Values being submitted and save data in the database.
     * @param $param
     */
    private function processValuesAndSaveData($params)
    {
       $oauth_am_default_role = trim( $params['oauth_am_default_role'] );
       $this->oauthUtility->setStoreConfig(OAuthConstants::MAP_MAP_BY, $params['oauth_am_account_matcher']);
          $this->oauthUtility->setStoreConfig(OAuthConstants::MAP_USERNAME, $params['oauth_am_username']);
          $this->oauthUtility->setStoreConfig(OAuthConstants::MAP_EMAIL, $params['oauth_am_email']);
       $this->oauthUtility->setStoreConfig(OAuthConstants::MAP_FIRSTNAME, $params['oauth_am_first_name']);
       $this->oauthUtility->setStoreConfig(OAuthConstants::MAP_LASTNAME, $params['oauth_am_last_name']);
       $this->oauthUtility->setStoreConfig(OAuthConstants::MAP_GROUP, $params['oauth_am_group_name']);

        $this->oauthUtility->setStoreConfig(OAuthConstants::MAP_DEFAULT_ROLE, $oauth_am_default_role);

        $oauth_am_dont_allow_unlisted_user_role 
            = isset( $params['oauth_am_dont_allow_unlisted_user_role'] ) ? "checked" : "unChecked";
        $mo_oauth_dont_create_user_if_role_not_mapped 
            = isset($params['mo_oauth_dont_create_user_if_role_not_mapped']) ? "checked" : "unchecked";
        $admin_role_mapping = $this->processAdminRoleMapping($params);
        $customer_role_mapping = $this->processCustomerRoleMapping($params);
        
        $this->oauthUtility->setStoreConfig(OAuthConstants::MAP_USERNAME, $params['oauth_am_username']);
        $this->oauthUtility->setStoreConfig(OAuthConstants::MAP_EMAIL, $params['oauth_am_email']);
        $this->oauthUtility->setStoreConfig(OAuthConstants::MAP_GROUP, $params['oauth_am_group_name']);
        $this->oauthUtility->setStoreConfig(OAuthConstants::UNLISTED_ROLE, $oauth_am_dont_allow_unlisted_user_role);
        $this->oauthUtility->setStoreConfig(OAuthConstants::CREATEIFNOTMAP, $mo_oauth_dont_create_user_if_role_not_mapped);
        $this->oauthUtility->setStoreConfig(OAuthConstants::ROLES_MAPPED, serialize($admin_role_mapping));
        $this->oauthUtility->setStoreConfig(OAuthConstants::GROUPS_MAPPED, serialize($customer_role_mapping));
    
    }


    /* ===================================================================================================
                        THE FUNCTION BELOW ARE PREMIUM PLUGIN SPECIFIC 
    ===================================================================================================
    */

    /**
    * Read and process the Roles saved by the
    * admin.
    * @param $params
    * @return array
    */
    private function processAdminRoleMapping($params)
    {
    $admin_role_mapping = array();
    $roles = $this->adminRoleModel->toOptionArray();
    foreach ($roles as $role) 
    {
        $attr = 'oauth_am_admin_attr_values_' . $role['value'];
        if(array_key_exists($attr,$params))
            $admin_role_mapping[$role['value']] = $params[$attr];
    }
    return $admin_role_mapping;
    }


    /**
    * Read and process the Groups saved by the
    * admin.
    * @param $params
    * @return array
    */
    private function processCustomerRoleMapping($params)
    {
        $customer_role_mapping = array();
        $groups = $this->userGroupModel->toOptionArray();
        foreach ($groups as $group) 
        {
            $attr = 'oauth_am_group_attr_values_' . $group['value'];
            if(array_key_exists($attr,$params))
                $customer_role_mapping[$group['value']] = $params[$attr];
        }
        return $customer_role_mapping;
    }

    /**
     * Is the user allowed to view the Attribute Mapping settings.
     * This is based on the ACL set by the admin in the backend.
     * Works in conjugation with acl.xml
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed(OAuthConstants::MODULE_DIR.OAuthConstants::MODULE_ATTR);
    }
}
