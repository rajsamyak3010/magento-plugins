<?php

namespace MiniOrange\OAuth\Controller\Actions;

use MiniOrange\OAuth\Helper\Curl;
use MiniOrange\OAuth\Helper\OAuthConstants;
use MiniOrange\OAuth\Helper\OAuthMessages;
use MiniOrange\OAuth\Helper\Exception\AccountAlreadyExistsException;
use MiniOrange\OAuth\Helper\Exception\OTPValidationFailedException;
use MiniOrange\OAuth\Helper\Exception\RequiredFieldsException;
use MiniOrange\OAuth\Helper\Exception\OTPRequiredException;
use MiniOrange\OAuth\Controller\Actions\BaseAdminAction;

/**
 * Handles processing of the validate OTP form. Takes the OTP 
 * entered by the user and sends it for validation. If validation
 * is successful then register him in the plugin otherwise
 * throw an error.
 */
class ValidateOTPAction extends BaseAdminAction
{
    private $REQUEST;

    /**
	 * Execute function to execute the classes function. 
     * 
	 * @throws \Exception
	 */
	public function execute()
	{
        $this->checkIfRequiredFieldsEmpty(array('submit'=>$this->REQUEST));
        $submit = $this->REQUEST['submit'];
        $txID = $this->oauthUtility->getStoreConfig(OAuthConstants::TXT_ID);
        $otp = $this->REQUEST['otp_token'];
        if($submit=="Back") 
            $this->goBackToRegistrationPage();
        else
            $this->validateOTP($txID,$otp);
    }


    /**
     * Function resets all the values in the database and sends 
     * the user back to the registration page for a fresh
     * activation of the plugin.
     */
    private function goBackToRegistrationPage()
    {
        $this->oauthUtility->setStoreConfig(OAuthConstants::OTP_TYPE,'');
        $this->oauthUtility->setStoreConfig(OAuthConstants::CUSTOMER_EMAIL,'');
        $this->oauthUtility->setStoreConfig(OAuthConstants::CUSTOMER_PHONE,'');
        $this->oauthUtility->setStoreConfig(OAuthConstants::REG_STATUS,'');
        $this->oauthUtility->setStoreConfig(OAuthConstants::TXT_ID,'');
    }


    /**
     * Function calls the Curl function to validate the OTP
     * entered by the admin. 
     */
    private function validateOTP($transactionID,$otpToken)
    {
        if(!array_key_exists('otp_token',$this->REQUEST) 
            || $this->REQUEST['otp_token']=="") throw new OTPRequiredException;
        $result = Curl::validate_otp_token($transactionID,$otpToken);
        $result = json_decode($result,true);
        if(strcasecmp($result['status'], 'SUCCESS') == 0)
            $this->handleOTPValidationSuccess($result);
        else
            $this->handleOTPValidationFailed();
    }


    /**
     * This function handles what should happen after successful 
     * validation of the OTP entered by the admin. Call the create customer
     * API to create user in miniOrange and fetch user customerKey, apiKey, etc.
     * 
     * @param $result
     */
    private function handleOTPValidationSuccess($result)
    {
        $companyName = $this->oauthUtility->getStoreConfig(OAuthConstants::CUSTOMER_NAME);
        $firstName = $this->oauthUtility->getStoreConfig(OAuthConstants::CUSTOMER_FNAME); 
        $lastName = $this->oauthUtility->getStoreConfig(OAuthConstants::CUSTOMER_LNAME); 
        $email = $this->oauthUtility->getStoreConfig(OAuthConstants::CUSTOMER_EMAIL); 
        $phone = $this->oauthUtility->getStoreConfig(OAuthConstants::CUSTOMER_PHONE); 
        $result = Curl::create_customer($email,$companyName,'',$phone,$firstName,$lastName);
        $result = json_decode($result,true);
        if( strcasecmp( $result['status'], 'SUCCESS' ) == 0 )
            $this->configureUserInMagento($result);
        else if(strcasecmp( $result['status'], 'CUSTOMER_USERNAME_ALREADY_EXISTS' ) == 0)
        {
            $this->oauthUtility->setStoreConfig(OAuthConstants::REG_STATUS,OAuthConstants::STATUS_VERIFY_LOGIN);
            throw new AccountAlreadyExistsException;
        }   
    }


    /**
     * After user is created in miniOrange store relevant information
     * in Magento database for future API calls and license 
     * verification.
     * 
     * @param $result
     */
    private function configureUserInMagento($result)
    {
        $this->oauthUtility->setStoreConfig(OAuthConstants::CUSTOMER_KEY,$result['id']);
        $this->oauthUtility->setStoreConfig(OAuthConstants::API_KEY,$result['apiKey']);
        $this->oauthUtility->setStoreConfig(OAuthConstants::TOKEN,$result['token']);
        $this->oauthUtility->setStoreConfig(OAuthConstants::OTP_TYPE,'');
        $this->oauthUtility->setStoreConfig(OAuthConstants::TXT_ID,'');
        $this->oauthUtility->setStoreConfig(OAuthConstants::REG_STATUS,OAuthConstants::STATUS_COMPLETE_LOGIN);
        $this->messageManager->addSuccessMessage(OAuthMessages::REG_SUCCESS);
    }


     /**
     * This function is called to handle what should happen
     * after sending of OTP fails for a phone number or email.
     * 
     * @param $content
     */
    private function handleOTPValidationFailed()
    {
        $this->oauthUtility->setStoreConfig(OAuthConstants::REG_STATUS,OAuthConstants::STATUS_VERIFY_EMAIL);
        throw new OTPValidationFailedException;
    }


    /** Setter for the request Parameter */
    public function setRequestParam($request)
    {
		$this->REQUEST = $request;
		return $this;
    }
}