var $m = jQuery.noConflict();
$m(document).ready(function() {
    $m(".btn-link").click(function() {
        $m(".collapse").slideUp("slow");
        if (!$m(this).next("div").is(':visible')) {
            $m(this).next("div").slideDown("slow");
        }
    });

});