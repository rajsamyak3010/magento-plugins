<?php

class MiniOrange_Api_Adminhtml_IndexController extends Mage_Adminhtml_Controller_Action
{
    /** @var MiniOrange_Api_Helper_MoApi */
    private $moUtility;
    /** @var Mage_Admin_Model_Session */
    private $adminSession;
    /** @var Mage_Adminhtml_Helper_Data */
    private $adminHelper;
    /** @var MiniOrange_Api_Helper_Messages */
    private $messageHandler;

    private $options = array (
        'registerNewUser',
        'validateNewUser',
        'resendOTP',
        'sendOTPPhone',
        'loginExistingUser',
        'resetPassword',
        'removeAccount',
        /** ====  THIS HAS BEEN ADDED FOR THE PREMIUM PLUGIN ONLY ==== **/
        'verifyLicenseKey'
    );

    /** MiniOrange_Api_Customer_ApiController constructor.
    * @throws Mage_Core_Exception
    */
    protected function _construct()
    {
        parent::_construct();
        $this->moUtility = MiniOrange_Api_Block_MoApi::_fetch(
            MiniOrange_Api_Block_MoApi::MO_UTILITY
        );
        $this->adminSession = MiniOrange_Api_Block_MoApi::_fetch(
            MiniOrange_Api_Block_MoApi::ADMIN_SESSION
        );
        $this->adminHelper = MiniOrange_Api_Block_MoApi::_fetch(
            MiniOrange_Api_Block_MoApi::ADMIN_HELPER
        );
        $this->messageHandler = MiniOrange_Api_Block_MoApi::_fetch(
            MiniOrange_Api_Block_MoApi::MESSAGE_HANDLER
        );
    }

    public function indexAction()
    {
        $params = $this->getRequest()->getParams();  //get params
        if($this->moUtility->isFormOptionBeingSaved($params)) // check if form options are being saved
        {
            $keys = array_values($params);
            $operation = array_intersect($keys, $this->options);
            if (count($operation) > 0) {  // route data and proccess
                $this->_route_data(array_values($operation)[0], $params);
            }
        }
		$this->loadLayout();
		$this->_addContent($this->getLayout()->createBlock('core/template'));
        $this->renderLayout();
    }


    /**
     * Route the request data to appropriate functions for processing.
     * Check for any kind of Exception that may occur during processing
     * of form post data. Call the appropriate action.
     *
     * @param $op refers to operation to perform
     * @param $params
     */
    private function _route_data($op,$params)
    {
        switch ($op)
        {
            case $this->options[4]:
                $this->existingUserAction($params);                        break;
            case $this->options[5]:
                $this->forgotPassAction($params);                          break;
            case $this->options[6]:
                $this->removeAccount();                                    break;
        }
    }


    private function removeAccount()
    {
        MiniOrange_Api_Helper_Data::storeConfigs([
            MiniOrange_Api_Helper_Data::CUSTOMER_EMAIL      =>  '',
            MiniOrange_Api_Helper_Data::CUSTOMER_KEY        =>  '',
            MiniOrange_Api_Helper_Data::CUSTOMER_API_KEY    =>  '',
            MiniOrange_Api_Helper_Data::CUSTOMER_TOKEN      =>  '',
            MiniOrange_Api_Helper_Data::TXT_ID              =>  '',
            MiniOrange_Api_Helper_Data::REG_STATUS          =>  MiniOrange_Api_Helper_Data::VERIFICATION_REQUIRED,
        ]);
        $this->redirect("*/*/index");
    }


    private function forgotPassAction($params)
    {
        $email = $params['email'];
        $response = MiniOrange_Api_Helper_Curl::forgot_password($email);
        $content = json_decode($response,true);
        if(strcasecmp($content['status'], 'SUCCESS') == 0){
            $this->messageHandler->displayMessage(
                'Your new password has been generated and sent to '.$email.'.',
                MiniOrange_Api_Helper_Messages::SUCCESS
            );
        } else {
            $this->messageHandler->displayMessage(
                'Sorry we encountered an error while reseting your password.',
                MiniOrange_Api_Helper_Messages::ERROR
            );
        }
        $this->redirect("*/*/index");
    }
	
	
	private function existingUserAction($params)
    {
		if(!MiniOrange_Api_Helper_MoApi::is_curl_installed()) {
		    $this->messageHandler->displayMessage(
                'cURL is not enabled. Please <a id="cURL" href="#cURLfaq">click here</a> to see how to enable cURL.',
                MiniOrange_Api_Helper_Messages::ERROR
            );
        } else {
            $email = $params['email'];
            $password = $params['password'];
            $submit = $params['submit'];
            if(strcasecmp($submit,"Submit") == 0){
                $this->fetchCustomerInfo($email,$password);
            }
        }
        $this->redirect("*/*/index");
	}


	private function fetchCustomerInfo($email,$password)
    {
        $content = MiniOrange_Api_Helper_Curl::get_customer_key($email,$password);
        $customerKey = json_decode($content, true);
        if(json_last_error() == JSON_ERROR_NONE) {
            $this->saveConfig($customerKey,$email);
            $this->messageHandler->displayMessage(
                'Login Successful. You can configure Social Login & Sharing now.',
                MiniOrange_Api_Helper_Messages::SUCCESS
            );
        } else {
            $this->messageHandler->displayMessage(
                'Invalid Credentials',
                MiniOrange_Api_Helper_Messages::ERROR
            );
        }
    }

	
	private function redirect($url)
    {
		$redirect = $this->adminHelper->getUrl($url);
		$this->getResponse()->setRedirect($redirect);
	}


	private function saveConfig($customerKey,$email)
    {
        MiniOrange_Api_Helper_Data::storeConfigs([
            MiniOrange_Api_Helper_Data::CUSTOMER_EMAIL      =>  $email,
            MiniOrange_Api_Helper_Data::CUSTOMER_KEY        =>  $customerKey['id'],
            MiniOrange_Api_Helper_Data::CUSTOMER_API_KEY    =>  $customerKey['apiKey'],
            MiniOrange_Api_Helper_Data::CUSTOMER_TOKEN      =>  $customerKey['token'],
            MiniOrange_Api_Helper_Data::TXT_ID              =>  '',
            MiniOrange_Api_Helper_Data::REG_STATUS          =>  MiniOrange_Api_Helper_Data::STATUS_COMPLETE_LOGIN,
        ]);
	}
}