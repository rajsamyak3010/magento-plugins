<?php

class MiniOrange_Api_Model_Observer extends Varien_Event_Observer
{
    /** @var MiniOrange_Api_Helper_MoApi */
    private $moUtility;


    /** MiniOrange_Api_Customer_ApiController constructor.
     * @throws Mage_Core_Exception
     */
    protected function _construct()
    {
        parent::_construct();
        $this->moUtility = MiniOrange_Api_Block_MoApi::_fetch(
            MiniOrange_Api_Block_MoApi::MO_UTILITY
        );
    }


    /**
     * @param Varien_Event_Observer $observer
     * @throws Zend_Controller_Request_Exception
     */
    public function controllerActionPredispatch(Varien_Event_Observer $observer)
    {
        $request = Mage::app()->getRequest();
        if($this->isApiCall($request)) {
            $hmacData = $this->getHmacData($request);
            $customerKeyInApi = $hmacData[0];
            $digestInApi = $hmacData[1];
            $timestamp = $request->getHeader("Timestamp");
            $method = $request->getMethod();
            $uri = $this->getEndPoint($request);

            $token = MiniOrange_Api_Helper_Data::getConfig(MiniOrange_Api_Helper_Data::CUSTOMER_TOKEN);
            $customerKey = MiniOrange_Api_Helper_Data::getConfig(MiniOrange_Api_Helper_Data::CUSTOMER_KEY);
            $customerApiKey = MiniOrange_Api_Helper_Data::getConfig(MiniOrange_Api_Helper_Data::CUSTOMER_API_KEY);

            $calDigest = base64_encode(
                hash_hmac("sha256", $method."+".$uri."+".$customerApiKey."+".$timestamp ,$token,true)
            );

//            var_dump($method."+".$uri."+".$timestamp);
//            var_dump($calDigest);
//            var_dump($digestInApi);
//            exit;

            if($calDigest!==$digestInApi || $customerKey!==$customerKeyInApi) {
                $request->setControllerName("api")->setActionName('error')->setDispatched(false);
            }
        }
	}

	private function getEndPoint(Mage_Core_Controller_Request_Http $request)
    {
        $uri = $request->getOriginalPathInfo();
        return substr($uri,strpos($uri,"/customers/api"),strlen($uri));
    }


    /**
     * @param Mage_Core_Controller_Request_Http $request
     * @return mixed
     * @throws Zend_Controller_Request_Exception
     */
    private function getHmacData(Mage_Core_Controller_Request_Http $request)
    {
        $authHeader = $request->getHeader("Authorization");
        $authHeader = trim(str_replace('hmac','',$authHeader));
        $authHeader = explode(":",$authHeader);
        return $authHeader;
    }


	private function isApiCall(Mage_Core_Controller_Request_Http $request)
    {
        $data = MiniOrange_Api_Block_MoApi::ENDPOINT_DATA;
        $data = array_keys($data["endpoints"]);
        return $request->getControllerModule()==="MiniOrange_Api_Customer" && in_array($request->getActionName(),$data);
    }
}