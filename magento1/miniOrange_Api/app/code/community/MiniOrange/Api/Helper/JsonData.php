<?php

class MiniOrange_Api_Helper_JsonData extends Mage_Core_Helper_Abstract
{
    const SUCCESS   = 'SUCCESS';
    const ERROR     = 'ERROR';
    const STATUS    = 'STATUS';
    const MESSAGE   = 'MESSAGE';
    const DATA      = 'DATA';
    const APP_TYPE  = 'Content-Type';
    const JSON      = 'application/json';

    public static function generateJSONData($message, $type, $data = [])
    {
        return json_encode([
            MiniOrange_Api_Helper_JsonData::STATUS => $type,
            MiniOrange_Api_Helper_JsonData::MESSAGE => $message,
            MiniOrange_Api_Helper_JsonData::DATA => $data
        ]);
    }


    /**
     * @param $statusCode
     * @param $jsonData
     * @param Mage_Core_Controller_Response_Http $response
     * @throws Zend_Controller_Response_Exception
     */
    public static function sendJSONData($statusCode, $jsonData, Mage_Core_Controller_Response_Http &$response)
    {
        $response
            ->clearHeaders()
            ->setHeader(
                MiniOrange_Api_Helper_JsonData::APP_TYPE,
                MiniOrange_Api_Helper_JsonData::JSON
            )
            ->setHttpResponseCode($statusCode)
            ->setBody($jsonData);
    }
}