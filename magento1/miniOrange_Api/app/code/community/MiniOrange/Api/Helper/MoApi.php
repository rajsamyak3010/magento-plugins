<?php

class MiniOrange_Api_Helper_MoApi extends Mage_Core_Helper_Abstract
{
    public static function is_curl_installed()
    {
        return in_array ( 'curl', get_loaded_extensions ());
    }

    /**
     * Check if form is being saved in the backend other just
     * show the page. Checks if the request parameter has
     * an option key. All our forms need to have a hidden option
     * key.
     *
     * @param array
     * @return bool
     */
    public function isFormOptionBeingSaved($params)
    {
        return array_key_exists('option',$params);
    }


    /**
     * This function checks if the required fields passed to
     * this function are empty or not. If empty throw an exception.
     *
     * @param $array
     * @return bool
     */
    public function checkIfRequiredFieldsEmpty($array)
    {
        foreach ($array as $key => $value)
        {
            if(
                (
                    is_array($value)
                    && (
                        !array_key_exists($key,$value) || self::isBlank($value[$key])
                    )
                )
                || self::isBlank($value)
            ){
                return false;
            }
        }
        return true;
    }


    /**
     * This function checks if a value is set or
     * empty. Returns true if value is empty
     *
     * @return True or False
     * @param $value Mixed references the variable passed.
     */
    public static function isBlank( $value )
    {
        if( ! isset( $value ) || empty( $value ) ) return TRUE;
        return FALSE;
    }
}