<?php
include Mage::getModuleDir('', 'MiniOrange_SamlSP').DS.'Helper'.DS.'Utilities.php';
include Mage::getModuleDir('', 'MiniOrange_SamlSP').DS.'Helper'.DS.'Response.php';
include Mage::getModuleDir('', 'MiniOrange_SamlSP').DS.'Helper'.DS.'LogoutRequest.php';
include Mage::getModuleDir('', 'MiniOrange_SamlSP').DS.'Helper'.DS.'encryption.php';
class MiniOrange_SamlSP_Model_Observer
{
	private $_helper1 = "MiniOrange_SamlSP"; 
	private $_helper2 = "MiniOrange_SamlSP/moSamlUtility";
	
	public function controllerActionPredispatch(Varien_Event_Observer $observer){
		$request = Mage::app()->getRequest();
		$helper = $this->getHelper2();	
		$data = $this->getHelper1();	
		if($request->getRequestedControllerName() == 'index' && $request->getRequestedActionName() == 'index'){
			if(array_key_exists('SAMLResponse', $_REQUEST) && !empty($_REQUEST['SAMLResponse'])) {
				$this->samlResponse($_REQUEST,$_GET);
			}else if(array_key_exists('SAMLRequest', $_REQUEST) && !empty($_REQUEST['SAMLRequest'])) {
				$this->readSamlRequest($_REQUEST,$_GET);
			}if( isset( $_REQUEST['option'] ) and strpos( $_REQUEST['option'], 'readsamllogin' ) !== false ) {
				//feature not ready yet
			}
		}else if($request->getRequestedControllerName() == 'index' && $request->getRequestedActionName() == 'login'){
			$tempUser = $data->getConfig('tempUser');
			$user = !empty($tempUser) ? Mage::getModel('admin/user')->load($data->getConfig('tempUser')) : '' ;
			if(!empty($user)){ 
				$this->adminLogin($user); 
			}else if($data->getConfig('loginRedirect') && !array_key_exists('saml_sso',$_REQUEST)){
				$this->sendLoginRequest(Mage::helper('core/url')->getCurrentUrl());
			}
		}else if($request->getRequestedControllerName() == 'adminhtml_index' && $request->getRequestedActionName() == 'samlLoginRequest'){
			if(!array_key_exists('q',$_REQUEST) || $_REQUEST['q']!='testConfig')
				$this->sendLoginRequest($_REQUEST['q']);
		}else if($request->getRequestedControllerName() == 'account' && $request->getRequestedActionName() == 'login' && $data->getConfig('loginRedirect')
			&& !array_key_exists('saml_sso',$_REQUEST) && $_REQUEST['saml_sso']!='false'){
				if(!Mage::getSingleton('core/session')->getRequestSent())
					$this->sendLoginRequest(Mage::helper('core/url')->getCurrentUrl());
				else
					Mage::getSingleton('core/session')->unsRequestSent();
		}
	}
	
	private function sendLoginRequest($sendRelayState){
		$helper = $this->getHelper1();
		$customer = $this->getHelper2();
		if($helper->mo_saml_is_sp_configured()){
			$ssoUrl = $helper->getConfig("loginUrl");
			$sso_binding_type =  $helper->getConfig("loginBindingType");				
			$force_authn = $helper->getConfig("forceAuthn"); 
			$cpBlock= $this->getBlock('MiniOrange_SamlSP_Block_MoSamlSPConfig');
			$acsUrl = $cpBlock->getBaseUrl();
			$issuer = $cpBlock->getIssuerUrl();
			
			$samlRequest = Utilities::createAuthnRequest($acsUrl, $issuer, $ssoUrl, $force_authn, $sso_binding_type);
			
			if(empty($sso_binding_type) || $sso_binding_type == 'HttpRedirect') {
				$samlRequest = "SAMLRequest=" . $samlRequest . "&RelayState=" . urlencode($sendRelayState) . '&SigAlg='. urlencode(XMLSecurityKey::RSA_SHA256);
				$param =array( 'type' => 'private');
				$key = new XMLSecurityKey(XMLSecurityKey::RSA_SHA256, $param);
				$certFilePath = $cpBlock->getResouceURL('sp-key.key');
				$key->loadKey($certFilePath, TRUE);
				$objXmlSecDSig = new XMLSecurityDSig();
				$signature = $key->signData($samlRequest);
				$signature = base64_encode($signature);
				$redirect = $ssoUrl;
				if (strpos($ssoUrl,'?') !== false) {
					$redirect .= '&';
				} else {
					$redirect .= '?';
				}
				$redirect .= $samlRequest . '&Signature=' . urlencode($signature);
				$response = Mage::app()->getFrontController()->getResponse();
				$response->setRedirect($redirect);
			} else {
				$privateKeyPath = $cpBlock->getResouceURL('sp-key.key');
				$publicCertPath = $cpBlock->getResouceURL('sp-certificate.crt');
				
				$base64EncodedXML = Utilities::signXML( $samlRequest, $publicCertPath, $privateKeyPath, 'NameIDPolicy' );
				Mage::getSingleton('core/session')->setRequestSent(1);
				Utilities::postSAMLRequest($ssoUrl, $base64EncodedXML, $sendRelayState);
			}
		}
	}
	
	private function readSamlRequest($REQUEST,$GET){
		$cpBlock = $this->getBlock('MiniOrange_SamlSP_Block_MoSamlSPConfig');
		$samlRequest = $REQUEST['SAMLRequest'];
		$relayState = '/';
		if(array_key_exists('RelayState', $REQUEST)) {
			$relayState = $REQUEST['RelayState'];
		}
		
		$samlRequest = base64_decode($samlRequest);
		if(array_key_exists('SAMLRequest', $GET) && !empty($GET['SAMLRequest'])) {
			$samlRequest = gzinflate($samlRequest);
		}
		
		$document = new DOMDocument();
		$document->loadXML($samlRequest);
		$samlRequestXML = $document->firstChild;
		
		if( $samlRequestXML->localName == 'LogoutRequest' ) {
			$logoutRequest = new SAML2_LogoutRequest( $samlRequestXML );
			$requestDetails = Array();
			if( Mage::getSingleton('customer/session')->isLoggedIn() ) {
				$this->mo_get_user_before_logout();
			}else{
				$this->mo_get_admin_before_logout();
			}
			$this->mo_saml_logout(null,true,$samlRequest,$relayState);
		}
		
	}
	
	private function samlResponse($POSTED,$GET){
		$datahelper = $this->getHelper1();
		$customer = $this->getHelper2();
		$cpBlock = $this->getBlock('MiniOrange_SamlSP_Block_MoSamlSPConfig');
		$session = $cpBlock->getSession();
		$sp_base_url = $cpBlock->getBaseUrl();
		$samlResponse = $POSTED['SAMLResponse'];
		$samlResponse = base64_decode($samlResponse);
		if(array_key_exists('SAMLResponse', $GET) && !empty($GET['SAMLResponse'])) {
			$samlResponse = gzinflate($samlResponse);
		}

		$document = new DOMDocument();
		$document->loadXML($samlResponse);
		$samlResponseXml = $document->firstChild;
		
		if($samlResponseXml->localName == 'LogoutResponse') {
			if(array_key_exists('RelayState', $POSTED) && !empty( $POSTED['RelayState'] ) && $POSTED['RelayState'] != '/') 
				Mage::app()->getResponse()->setRedirect($POSTED['RelayState']);
			else
				Mage::app()->getResponse()->setRedirect($cpBlock->getBaseUrl());
		} else {
			// It's a SAML Assertion
			if(array_key_exists('RelayState', $POSTED) && !empty( $POSTED['RelayState'] ) && $POSTED['RelayState'] != '/') {
				$relayState = $POSTED['RelayState'];
			} else {
				$relayState = '';
			}
			
			$certFromPlugin = $datahelper->getConfig('certificate');
			$certfpFromPlugin = XMLSecurityKey::getRawThumbprint($certFromPlugin);
			
			$acsUrl = $cpBlock->getBaseUrl();
			$samlResponse = new SAML2_Response($samlResponseXml);
			
			$responseSignatureData = $samlResponse->getSignatureData();
			$assertionSignatureData = current($samlResponse->getAssertions())->getSignatureData();

			/* convert to UTF-8 character encoding*/
			$certfpFromPlugin = iconv("UTF-8", "CP1252//IGNORE", $certfpFromPlugin);
			
			/* remove whitespaces */
			$certfpFromPlugin = preg_replace('/\s+/', '', $certfpFromPlugin);	
			
			$responseSignedOption = $datahelper->getConfig('responseSigned');
			$assertionSignedOption = $datahelper->getConfig('assertionSigned');
 			
			/* Validate signature */
			if($responseSignedOption) {
				$validSignature = Utilities::processResponse($acsUrl, $certfpFromPlugin, $responseSignatureData, $samlResponse);
				if($validSignature === FALSE) {
					echo "Invalid signature in the SAML Response.";
					exit;
				}
			}
			
			if($assertionSignedOption) {
				$validSignature = Utilities::processResponse($acsUrl, $certfpFromPlugin, $assertionSignatureData, $samlResponse);
				if($validSignature === FALSE) {
					echo "Invalid signature in the SAML Assertion.";
					exit;
				}
			}
			
			// verify the issuer and audience from saml response
			$issuer = $datahelper->getConfig('samlIssuer');
			$spEntityId = $cpBlock->getIssuerUrl();
			Utilities::validateIssuerAndAudience($samlResponse,$spEntityId, $issuer);
			
			$ssoemail = current(current($samlResponse->getAssertions())->getNameId());
			$attrs = current($samlResponse->getAssertions())->getAttributes();
			$attrs['NameID'] = array("0" => $ssoemail);
			$sessionIndex = current($samlResponse->getAssertions())->getSessionIndex();
			
			$this->mo_saml_checkMapping($attrs,$relayState,$sessionIndex);
		}
    }
	
	private function mo_saml_checkMapping($attrs,$relayState,$sessionIndex){
		try {
			$helper = $this->getHelper1();
			$customer = $this->getHelper2();
			
			$emailAttribute = $helper->getConfig('amEmail');
			$usernameAttribute = $helper->getConfig('amUsername');
			$firstName = $helper->getConfig('amFirstName');
			$lastName = $helper->getConfig('amLastName');
			$groupName = $helper->getConfig('amGroupName');
			$defaultRole = $helper->getConfig('defaultRole');
			$dontAllowUnlistedUserRole = $helper->getConfig('unlistedUserRole');
			$checkIfMatchBy = $helper->getConfig('amAccountMatcher');
			$user_email = '';
			$userName = '';
			
			if(is_null($defaultRole)){
				$defaultRole = 'General';
			}

			if(!empty($attrs)){
				if(!empty($firstName) && array_key_exists($firstName, $attrs))
					$firstName = $attrs[$firstName][0];
				else
					$firstName = $attrs['NameID'][0];

				if(!empty($lastName) && array_key_exists($lastName, $attrs))
					$lastName = $attrs[$lastName][0];
				else
					$lastName = '';
				
				if(empty($checkIfMatchBy)) {
					$checkIfMatchBy = "email";
				}

				if(!empty($usernameAttribute) && array_key_exists($usernameAttribute, $attrs))
					$userName = $attrs[$usernameAttribute][0];
				else
					$userName = $checkIfMatchBy=='username' ? $attrs['NameID'][0] : null;

				if(!empty($emailAttribute) && array_key_exists($emailAttribute, $attrs))
					$user_email = $attrs[$emailAttribute][0];
				else
					$user_email = $checkIfMatchBy=='email' ? $attrs['NameID'][0] : null;
				
				if(!empty($groupName) && array_key_exists($groupName, $attrs))
					$groupName = $attrs[$groupName];
				else
					$groupName = array();
			}
			
			if($relayState=='testValidate'){
				$this->mo_saml_show_test_result($firstName,$lastName,$user_email,$groupName,$attrs);
			}else{
				$this->mo_saml_login_user($user_email, $firstName, $lastName, $userName, $groupName, $dontAllowUnlistedUserRole, $defaultRole, $relayState, $checkIfMatchBy, $sessionIndex, $attrs['NameID'][0], $attrs);
			}
		}
		catch (Exception $e) {
			echo sprintf("An error occurred while processing the SAML Response.");
			exit;
		}
	}
	
	private function mo_saml_login_user($user_email=null, $firstName, $lastName, $userName=null, $groupName, $dontAllowUnlistedUserRole, $defaultRole, $relayState, $checkIfMatchBy, $sessionIndex = '', $nameId = '', $attrs = null){
		$helper = $this->getHelper1();
		$customer = $this->getHelper2();
		$cpBlock = $this->getBlock('MiniOrange_SamlSP_Block_MoSamlSPConfig');
		$admin = false;
		
		$roles = Mage::getModel('admin/roles')->getCollection();
		$groups = Mage::helper('customer')->getGroups();
		
		if($checkIfMatchBy=='email')
			$user = Mage::getModel('admin/user')->getCollection()->addFieldToFilter('email',$user_email)->getFirstItem();
		else
			$user = Mage::getModel('admin/user')->loadByUsername($userName);
		
		if(!is_null($user->getUserId())){
			$admin = true;
		}else{
			$user = Mage::getModel("customer/customer")->getCollection()->addFieldToFilter('email',$user_email)->getFirstItem();
		}
	
		if(!is_null($user->getUserId()) || !is_null($user->getId())) {
			$user_id = $user->getUserId();
			$user_id = !is_null($user_id) ?  $user_id : $user->getId();
			if( !empty($firstName) ){
				$this->saveConfig('firstname',$firstName,$user_id,$admin);
			}
			if( !empty($lastName) ){
				$this->saveConfig('lastname',$lastName,$user_id,$admin);
			}
			
			if($admin)
				$role_mapping = unserialize($helper->getConfig('samlAdminRoleMapping'));
			else
				$role_mapping = unserialize($helper->getConfig('samlCustomerRoleMapping'));
			
			$role_assigned = $this->assign_roles_to_user( $user_id, $role_mapping, $groupName );
			
			if(!empty($role_assigned) && !empty($dontAllowUnlistedUserRole) && $dontAllowUnlistedUserRole == 'checked') {
				//donot assign any role
			}elseif(!empty($role_assigned)) {
				if($admin)
					$user->setRoleIds($role_assigned)->setRoleUserId($user->getUserId())->saveRelations();
				else{
					$user->setData('group_id',$role_assigned[0]); // customer cannot have multiple groups
					$user->save();
				}
			}
			
			/*if(!is_null( $attrs )) {
				update_user_meta( $user_id, 'mo_saml_user_attributes', $attrs);
			}*/
			
			if(!empty( $sessionIndex)) {
				$this->saveConfig('mo_saml_session_index',$sessionIndex,$user_id,$admin);
			}
			if(!empty( $nameId )) {
				$this->saveConfig('mo_saml_name_id',$nameId,$user_id,$admin);
			}
			
			if($admin){
				$storeConfig = new Mage_Core_Model_Config();
				$storeConfig ->saveConfig('samlsp/temp/user',$user->getUserId(),'default', 0);
				$redirectUrl = $cpBlock->getAdminLoginUrl();
				$response = Mage::app()->getFrontController()->getResponse();
				$response->setRedirect($redirectUrl);
			}else{
				$this->customerLogin($user,$checkIfMatchBy,$relayState);
			}
			
		}else{

			$role_mapping1 = unserialize($helper->getConfig('samlAdminRoleMapping'));
			$role_mapping2 = unserialize($helper->getConfig('samlCustomerRoleMapping'));
			$role_mapping = array_merge($role_mapping1,$role_mapping2);
			$is_create_user = true;
			
			$dont_create_user_if_role_not_mapped = $helper->getConfig('createUserIfRoleNotMapped');	
			if (!empty($dont_create_user_if_role_not_mapped) && strcmp( $dont_create_user_if_role_not_mapped, 'checked') == 0 ) {
				$role_configured = $this->is_role_mapping_configured_for_user( $role_mapping, $groupName );
				$is_create_user = $role_configured;
			}
			
			if($is_create_user === true){
				$random_password = Mage::helper('core')->getRandomString($length = 8);
				$username = !is_null($userName)? $userName : $user_email;
				$siteurl = $cpBlock->getBaseUrl();
				$siteurl = substr($siteurl,strpos($siteurl,'//'),strlen($siteurl)-1);
				$email = !is_null($user_email)? $user_email : $username .'@'.$siteurl;
				$websiteId = Mage::app()->getWebsite()->getId();
				$store = Mage::app()->getStore();
				
				$role_assigned = $this->assign_roles_to_user( null, $role_mapping, $groupName );
				
				
				
				if(!empty($role_assigned) && !empty($dontAllowUnlistedUserRole) && $dontAllowUnlistedUserRole == 'checked') {
					//do not assign any role
				}elseif(!empty($role_assigned)) {
					
					foreach($roles as $role):
						$admin = $role_assigned[0]==$role->getRoleId() ? true : false;
						if($admin){break;}
					endforeach; 
					
					if(!$admin){
						foreach($groups as $group):
							$customer = $role_assigned[0]==$group->getCustomerGroupId() ? true : false;
							if($customer){break;}
						endforeach;
					}

					if($admin){
						$user = $this->createAdminUser($userName,$firstName,$lastName,$email,$random_password,$role_assigned);
						$user_id = $user->getId();
					}else{
						$user = $this->createCustomer($userName,$firstName,$lastName,$email,$random_password,$role_assigned);
						$user_id = $user->getUserId();
					}
				} elseif(!is_null($defaultRole)) {
					$setDefaultRole = array();
					foreach($roles as $role):
						$admin = $defaultRole==$role->getRoleName()? true : false;
						if($admin){ array_push($setDefaultRole,$role->getRoleId() ); break; }
					endforeach; 

					if(!$admin){
						foreach($groups as $group):
							$customer = $defaultRole==$group->getCustomerGroupCode()? true : false;
							if($customer){ array_push($setDefaultRole,$group->getCustomerGroupId()); break; } 
						endforeach;
					}
					
					if($admin){						
						$user = $this->createAdminUser($userName,$firstName,$lastName,$email,$random_password,$setDefaultRole);
						$user_id = $user->getId();
					}else{
						$user = $this->createCustomer($userName,$firstName,$lastName,$email,$random_password,$setDefaultRole);
						$user_id = $user->getUserId();
					}
					
				}
					
				/*if(!is_null( $attrs )) {
					update_user_meta( $user_id, 'mo_saml_user_attributes', $attrs);
				}*/
				
				if(!empty( $sessionIndex)) {
					$this->saveConfig('mo_saml_session_index',$sessionIndex,$user_id,$admin);
				}
				if(!empty( $nameId )) {
					$this->saveConfig('mo_saml_name_id',$nameId,$user_id,$admin);
				}
				
				if($admin){
					$storeConfig = new Mage_Core_Model_Config();
					$storeConfig ->saveConfig('samlsp/temp/user',$user->getUserId(),'default', 0);
					$redirectUrl = $cpBlock->getAdminLoginUrl();
					$response = Mage::app()->getFrontController()->getResponse();
					$response->setRedirect($redirectUrl);
				}else{
					$this->customerLogin($user,$checkIfMatchBy,$relayState);
				}
			}
		}
	}
	
	private function mo_saml_show_test_result($firstName,$lastName,$user_email,$groupName,$attrs){
		$cpBlock = $this->getBlock('MiniOrange_SamlSP_Block_MoSamlSPConfig');
		ob_end_clean();
		echo '<div style="font-family:Calibri;padding:0 3%;">';
		if(!empty($user_email)){
			echo '<div style="color: #3c763d;
					background-color: #dff0d8; padding:2%;margin-bottom:20px;text-align:center; border:1px solid #AEDB9A; font-size:18pt;">TEST SUCCESSFUL</div>
					<div style="display:block;text-align:center;margin-bottom:4%;"><img style="width:15%;"src="'.$cpBlock->getImage('right').'"></div>';
		}else{
			echo '<div style="color: #a94442;background-color: #f2dede;padding: 15px;margin-bottom: 20px;text-align:center;border:1px solid #E6B3B2;font-size:18pt;">TEST FAILED</div>
					<div style="color: #a94442;font-size:14pt; margin-bottom:20px;">WARNING: Some Attributes Did Not Match.</div>
					<div style="display:block;text-align:center;margin-bottom:4%;"><img style="width:15%;"src="'.$cpBlock->getImage('wrong').'"></div>';
		}
			echo '<span style="font-size:14pt;"><b>Hello</b>, '.$user_email.'</span><br/><p style="font-weight:bold;font-size:14pt;margin-left:1%;">ATTRIBUTES RECEIVED:</p>
					<table style="border-collapse:collapse;border-spacing:0; display:table;width:100%; font-size:14pt;background-color:#EDEDED;">
					<tr style="text-align:center;"><td style="font-weight:bold;border:2px solid #949090;padding:2%;">ATTRIBUTE NAME</td><td style="font-weight:bold;padding:2%;border:2px solid #949090; word-wrap:break-word;">ATTRIBUTE VALUE</td></tr>';
		if(!empty($attrs))
			foreach ($attrs as $key => $value)
				echo "<tr><td style='font-weight:bold;border:2px solid #949090;padding:2%;'>" .$key . "</td><td style='padding:2%;border:2px solid #949090; word-wrap:break-word;'>" .implode("<br/>",$value). "</td></tr>";
			else
				echo "No Attributes Received.";
			echo '</table></div>';
			echo '<div style="margin:3%;display:block;text-align:center;"><input style="padding:1%;width:100px;background: #0091CD none repeat scroll 0% 0%;cursor: pointer;font-size:15px;border-width: 1px;border-style: solid;border-radius: 3px;white-space: nowrap;box-sizing: border-box;border-color: #0073AA;box-shadow: 0px 1px 0px rgba(120, 200, 230, 0.6) inset;color: #FFF;"type="button" value="Done" onClick="self.close();"></div>';
			exit;
	}
	
	public function mo_get_admin_before_logout(){
		$switchSessionName = 'adminhtml';
		$currentSessionId = Mage::getSingleton('core/session')->getSessionId();
		$currentSessionName = Mage::getSingleton('core/session')->getSessionName();
		if ($currentSessionId && $currentSessionName && isset($_COOKIE[$currentSessionName])) {
			$switchSessionId = $_COOKIE[$switchSessionName];
			$this->_switchSession($switchSessionName, $switchSessionId);
			$admin = Mage::getModel('admin/session')->getUser();
			if(!empty($admin) && $admin->getId()) {
				$data = array();
				$data['admin'] = true;
				$data['id'] =$admin->getId();
				Mage::getSingleton('core/session')->setUserDetails($data);
			}
			$this->_switchSession($currentSessionName, $currentSessionId);
		}
		
	}
	
	protected function _switchSession($namespace, $id = null) {
		session_write_close();
		$GLOBALS['_SESSION'] = null;
		$session = Mage::getSingleton('core/session');
		if ($id) {
			$session->setSessionId($id);
		}
		$session->start($namespace);
	}
	
	public function mo_get_user_before_logout(){
		$cpBlock = $this->getBlock('MiniOrange_SamlSP_Block_MoSamlSPConfig');
		$user = $cpBlock->getSession()->getCustomer();
		if(!empty($user) && $user->getId()) {
			$data = array();
			$data['admin'] = false;
			$data['id'] =$user->getId();
			Mage::getSingleton('core/session')->setUserDetails($data);
		}
	}
	
	public function mo_saml_logout(Varien_Event_Observer $observer, $mo_saml_logout_request=false, $samlRequest = null,$relayState = null) {
		$redirectDetails = Array();
		$data = $this->getHelper1();
		$customer = $this->getHelper2();
		$cpBlock = $this->getBlock('MiniOrange_SamlSP_Block_MoSamlSPConfig');
		$userdata= Mage::getSingleton('core/session')->getUserDetails();
		$admin = array_key_exists('admin',$userdata) ? $userdata['admin'] : 0;
		$id = array_key_exists('id',$userdata) ? $userdata['id'] : 0;
		Mage::getSingleton('core/session')->unsUserDetails();
		
		$logout_url = $data->getConfig('logoutUrl');
		$logout_binding_type = $data->getConfig('logoutBindingType');
		$redirectDetails = Mage::getSingleton('core/session')->getLogoutResponseRedirect();
		if(!empty($redirectDetails) && array_key_exists('redirect',$redirectDetails)){
			$this->sendCustomerLogoutResponse($logout_url, $redirectDetails['binding'], $redirectDetails['redirect'], $redirectDetails['relayState']);
			exit();
		}
		else if($id && !empty($logout_url) ){
			if($mo_saml_logout_request) {
				$redirectDetails = Mage::getSingleton('core/session')->getLogoutResponseRedirect();
				if(array_key_exists('customer',$redirectDetails))
					$this->sendCustomerLogoutResponse($logout_url, $redirectDetails['binding'], $redirectDetails['redirect'], $redirectDetails['relayState']);
				else
					$this->createLogoutResponseAndRedirect($logout_url, $logout_binding_type, $samlRequest, $relayState, $admin);
			}else{
			
				if($admin){
					$nameId = $data->getConfig('adminNameID',$id);
					$sessionIndex =  $data->getConfig('adminSessionIndex',$id);
					$sp_base_url = $cpBlock->getAdminLoginUrl();
				}else{
					$nameId = $data->getConfig('customerNameID',$id);
					$sessionIndex =  $data->getConfig('customerSessionIndex',$id);
					$sp_base_url = $cpBlock->getBaseUrl();
				}
				
				$this->saveConfig('mo_saml_name_id','',$id,$admin);
				$this->saveConfig('mo_saml_session_index','',$id,$admin);
					
				$issuer = $cpBlock->getIssuerUrl();
				$destination = $logout_url;
				$sendRelayState = $sp_base_url;
				$samlRequest = Utilities::createLogoutRequest($nameId, $sessionIndex, $issuer, $destination, $logout_binding_type);
				
				if(empty($logout_binding_type) || $logout_binding_type == 'HttpRedirect') {
					$samlRequest = 'SAMLRequest=' . $samlRequest . '&RelayState=' . urlencode($sendRelayState) . '&SigAlg='. urlencode(XMLSecurityKey::RSA_SHA256);
					$param =array( 'type' => 'private');
					$key = new XMLSecurityKey(XMLSecurityKey::RSA_SHA256, $param);
					//$certFilePath = plugins_url('resources/sp-key.key', __FILE__) ;
					$certFilePath = $cpBlock->getResouceURL('sp-key.key');
					$key->loadKey($certFilePath, TRUE);
					$objXmlSecDSig = new XMLSecurityDSig();
					$signature = $key->signData($samlRequest);
					$signature = base64_encode($signature);
					$redirect = $logout_url;
					if (strpos($logout_url,'?') !== false) {
						$redirect .= '&';
					} else {
						$redirect .= '?';
					}
					$redirect .= $samlRequest . '&Signature=' . urlencode($signature);					
					$response = Mage::app()->getFrontController()->getResponse();
					$response->setRedirect($redirect);
				} else {
					$privateKeyPath = $cpBlock->getResouceURL('sp-key.key');
					$publicCertPath = $cpBlock->getResouceURL('sp-certificate.crt');
					
					$base64EncodedXML = Utilities::signXML( $samlRequest, $publicCertPath, $privateKeyPath, 'NameID' );
					Utilities::postSAMLRequest($logout_url, $base64EncodedXML, $sendRelayState);
				}
			}
		}
	}
	
	private function getBlock($value){
		return Mage::getSingleton('core/layout')->getBlockSingleton($value);
	}
	
	private function getHelper1(){
		return Mage::helper($this->_helper1);
	}
	
	private function getHelper2(){
		return Mage::helper($this->_helper2);
	}
	
	private function customerLogin($user,$type,$relayState){
		$cpBlock = $this->getBlock('MiniOrange_SamlSP_Block_MoSamlSPConfig');
		$session = Mage::getSingleton('customer/session');
		$session->setCustomerAsLoggedIn($user);
		if ($session->isLoggedIn()) {
				$url = $cpBlock->getBaseUrl().'customer/account/';
				$response = Mage::app()->getFrontController()->getResponse();
				$response->setRedirect($url);
		}
	}
	
	private function adminLogin($user){
		$cpBlock = $this->getBlock('MiniOrange_SamlSP_Block_MoSamlSPConfig');
		if (Mage::getSingleton('adminhtml/url')->useSecretKey()) {
		  Mage::getSingleton('adminhtml/url')->renewSecretUrls();
		}
		
		$session = Mage::getSingleton('admin/session');
		$session->setIsFirstVisit(true);
		$session->setUser($user);
		$session->setAcl(Mage::getResourceModel('admin/acl')->loadAcl());

		Mage::dispatchEvent('admin_session_user_login_success',array('user'=>$user));
		
		$storeConfig = new Mage_Core_Model_Config();
		$storeConfig ->saveConfig('samlsp/temp/user','','default', 0);
		
		if ($session->isLoggedIn()) {
			$redirectUrl = Mage::getSingleton('adminhtml/url')->getUrl(Mage::getModel('admin/user')->getStartupPageUrl(), array('_current' => false));
			$response = Mage::app()->getFrontController()->getResponse();
			$response->setRedirect($redirectUrl);
		}
	}
	
	private function is_role_mapping_configured_for_user( $role_mapping, $groupName ) {
		if(!empty($groupName) && !empty($role_mapping)) {
			foreach ($role_mapping as $role_value => $group_names) {
				$groups = explode(";", $group_names);
				foreach ($groups as $group) {
					if(in_array($group, $groupName)) {
						return true;
					}
				}
			}
		}
		return false;
	}
	
	private function assign_roles_to_user( $user, $role_mapping, $groupName ) {
		$role = Array();
		$cpBlock = $this->getBlock('MiniOrange_SamlSP_Block_MoSamlSPConfig');
		if(!empty($groupName) && !empty($role_mapping) && !$cpBlock->is_administrator_user($user)) {
			foreach ($role_mapping as $role_value => $group_names) {
				$groups = explode(";", $group_names);
				foreach ($groups as $group) {
					if(in_array($group, $groupName)) {
						array_push($role,$role_value);
					}
				}
			}
		}
		return $role;
	}
	
	private function createLogoutResponseAndRedirect( $logout_url, $logout_binding_type, $logout_request, $relay_state, $admin) {
		
		$cpBlock = $this->getBlock('MiniOrange_SamlSP_Block_MoSamlSPConfig');
		$document = new DOMDocument();
		$document->loadXML($logout_request);
		$logout_request = $document->firstChild;

		if( $logout_request->localName == 'LogoutRequest' ) {
			$logoutRequest = new SAML2_LogoutRequest( $logout_request );
			$issuer = $cpBlock->getIssuerUrl();
			$destination = $logout_url;
			$redirectDetails = Array();
			
			$logoutResponse = Utilities::createLogoutResponse($logoutRequest->getId(), $issuer, $destination, $logout_binding_type);
			
			if(empty($logout_binding_type) || $logout_binding_type == 'HttpRedirect') {
				$redirect = $logout_url;
				if (strpos($logout_url,'?') !== false) {
					$redirect .= '&';
				} else {
					$redirect .= '?';
				}
				$redirect .= 'SAMLResponse=' . $logoutResponse . '&RelayState=' . urlencode($relay_state);
				
				if($admin){
					$switchSessionName = 'adminhtml';
					$currentSessionId = Mage::getSingleton('core/session')->getSessionId();
					$currentSessionName = Mage::getSingleton('core/session')->getSessionName();
					if ($currentSessionId && $currentSessionName && isset($_COOKIE[$currentSessionName])) {
						$switchSessionId = $_COOKIE[$switchSessionName];
						$this->_switchSession($switchSessionName, $switchSessionId);
						$adminSession = $cpBlock->getSession();
						$adminSession->unsetAll();
						$adminSession->getCookie()->delete($adminSession->getSessionName());
						$this->_switchSession($currentSessionName, $currentSessionId);
					}
					$response = Mage::app()->getFrontController()->getResponse();
					$response->setRedirect($redirect);
				}else{
					$redirectDetails['redirect']=$logoutResponse;
					$redirectDetails['binding']=$logout_binding_type;
					$redirectDetails['relayState']=$relay_state;
					$redirectDetails['customer']=true;
					Mage::getSingleton('core/session')->setLogoutResponseRedirect($redirectDetails);
					$cpBlock->getSession()->logout();
				}
			} else {
				$privateKeyPath = $cpBlock->getResouceURL('sp-key.key');
				$publicCertPath = $cpBlock->getResouceURL('sp-certificate.crt');
				
				$base64EncodedXML = Utilities::signXML( $logoutResponse, $publicCertPath, $privateKeyPath, 'Status' );
				
				if($admin){
					$adminSession = $cpBlock->getSession();
					$adminSession->unsetAll();
					$adminSession->getCookie()->delete($adminSession->getSessionName());
					Utilities::postSAMLResponse($logout_url, $base64EncodedXML, $relay_state);
				}else{
					$redirectDetails['redirect']=$logoutResponse;
					$redirectDetails['binding']=$logout_binding_type;
					$redirectDetails['relayState']=$relay_state;
					Mage::getSingleton('core/session')->setLogoutResponseRedirect($redirectDetails);
					$cpBlock->getSession()->logout();
				}
				
			}
		}
	}
	
	private function sendCustomerLogoutResponse($logout_url, $logout_binding_type,$logoutResponse, $relay_state){
		Mage::getSingleton('core/session')->unsLogoutResponseRedirect();
		$cpBlock = $this->getBlock('MiniOrange_SamlSP_Block_MoSamlSPConfig');
		if(empty($logout_binding_type) || $logout_binding_type == 'HttpRedirect') {
			$redirect = $logout_url;
			if (strpos($logout_url,'?') !== false) {
				$redirect .= '&';
			} else {
				$redirect .= '?';
			}
			$redirect .= 'SAMLResponse=' . $logoutResponse . '&RelayState=' . urlencode($relay_state);
			$response = Mage::app()->getFrontController()->getResponse();
			$response->setRedirect($redirect);
		}else{
			$privateKeyPath = $cpBlock->getResouceURL('sp-key.key');
			$publicCertPath = $cpBlock->getResouceURL('sp-certificate.crt');
				
			$base64EncodedXML = Utilities::signXML( $logoutResponse, $publicCertPath, $privateKeyPath, 'Status' );
			Utilities::postSAMLResponse($logout_url, $base64EncodedXML, $relay_state);
		}
	}
	
	private function saveConfig($url,$value,$id,$admin){
		$data = array($url=>$value);
		$model = $admin ? Mage::getModel('admin/user')->load($id)->addData($data) : Mage::getModel('customer/customer')->load($id)->addData($data);
		try {
			$model->setId($id)->save(); 
		} catch (Exception $e){
			Mage::log($e->getMessage(), null, 'miniorange_error.log', true);
		}
	}
	
	private function createAdminUser($userName,$firstName,$lastName,$email,$random_password,$role_assigned){
		$user = Mage::getModel('admin/user')->setData(array(
			'username'  => $userName,
			'firstname' => $firstName,
			'lastname'  => $lastName,
			'email'     => $email,
			'password'  => $random_password,
			'is_active' => 1
		))->save();
		$user_id=$user->getUserId();
		$user->setRoleIds($role_assigned)->setRoleUserId($user_id)->saveRelations();
		return $user;
	}
	
	private function createCustomer($userName,$firstName,$lastName,$email,$random_password,$role_assigned){
		$websiteId = Mage::app()->getWebsite()->getId();
		$store = Mage::app()->getStore();
		$user = Mage::getModel("customer/customer");
		$user   ->setWebsiteId($websiteId)
				->setStore($store)
				->setFirstname($firstName)
				->setLastname($lastName)
				->setEmail($email)
				->setPassword($random_password);
		$user->save();
		$user_id=$user->getId();

		if(is_array($role_assigned))
			$assign_role = $role_assigned[0];
		else
			$assign_role = $role_assigned;
		$user->setData('group_id',$assign_role); // customer cannot have multiple groups
		$user->save();
		
		return $user;
	}
}